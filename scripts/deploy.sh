#!/usr/bin/env bash

pip install awscli aws-sam-cli

original_sam_yaml=morning-workout.yaml
packaged_sam_yaml=packaged-morning-workout.yaml

mkdir src_temp
cp -r src/* src_temp
cd src && ls && pip3 install -r requirements.txt --target . && cd ..

AWS_DEFAULT_REGION=us-west-1 sam validate -t $original_sam_yaml --profile "personal"
aws s3 mb s3://morning-workout-stack --profile "personal"
sam package --template-file $original_sam_yaml --s3-bucket "morning-workout-stack" --output-template-file $packaged_sam_yaml --profile "personal"
sam deploy --template-file $packaged_sam_yaml --stack-name "morning-workout" --region "eu-west-1" --profile "personal" --capabilities CAPABILITY_IAM

rm $packaged_sam_yaml
rm -r src/*
cp -r src_temp/* src
rm -r src_temp

aws s3 mb s3://moring-workout-songs --profile "personal"

files_description=`aws s3 ls s3://moring-workout-songs --profile personal`

files_to_upload=`cd resources/ && ls`

s3_bucket_needs_update=false

for file_to_upload in $files_to_upload
do
  if [[ $files_description != *$file_to_upload* ]]; then
    s3_bucket_needs_update=true
  fi
done

if [[ "$s3_bucket_needs_update" == true ]]; then
  echo 'Resource files will be uploaded'
  aws s3 cp resources s3://moring-workout-songs --recursive --profile "personal"
else
  echo 'All files are up to date'
fi
