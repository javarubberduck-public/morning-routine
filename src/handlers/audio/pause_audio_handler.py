from ask_sdk_core.utils import is_intent_name
from ask_sdk_model.interfaces.audioplayer.stop_directive import StopDirective
from ask_sdk_runtime.dispatch_components import AbstractRequestHandler


class PauseAudioRequestHandler(AbstractRequestHandler):
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return is_intent_name('AMAZON.PauseIntent')(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        return handler_input.response_builder.add_directive(StopDirective()).response
