from ask_sdk_core.utils import is_intent_name, get_slot_value
from ask_sdk_model.interfaces.audioplayer.audio_item import AudioItem
from ask_sdk_model.interfaces.audioplayer.audio_item_metadata import \
    AudioItemMetadata
from ask_sdk_model.interfaces.audioplayer.play_behavior import PlayBehavior
from ask_sdk_model.interfaces.audioplayer.play_directive import PlayDirective
from ask_sdk_model.interfaces.audioplayer.stream import Stream
from ask_sdk_runtime.dispatch_components import AbstractRequestHandler


class ExercisesVersionRequestHandler(AbstractRequestHandler):
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return is_intent_name("exercisesVersion")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        version_number = get_slot_value(handler_input, "versionNumber")
        speech = f'<voice name="Matthew">Ok</voice>'
        response_builder = handler_input.response_builder.speak(
            speech).set_should_end_session(False)
        attr = handler_input.attributes_manager.session_attributes
        attr["state"] = "version_of_exercises_is_chosen"
        attr["counter"] = 0

        url = f'https://s3-eu-west-1.amazonaws.com/moring-workout-songs/version{version_number}.mp3'
        response_builder.add_directive(PlayDirective(
            PlayBehavior.REPLACE_ALL,
            AudioItem(
                Stream(
                    token='1',
                    url=url,
                    offset_in_milliseconds=0
                ),
                AudioItemMetadata(
                    title='Morning exercise',
                    subtitle=f'version {version_number}'
                )
            )
        ))

        return response_builder.response
