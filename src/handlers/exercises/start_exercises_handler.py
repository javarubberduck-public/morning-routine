from ask_sdk_core.utils import is_intent_name
from ask_sdk_runtime.dispatch_components import AbstractRequestHandler


class StartExercisesRequestHandler(AbstractRequestHandler):
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return is_intent_name("startExercises")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speech = '<voice name="Matthew">Which version of exercises do you ' \
                 'want to start?</voice>'
        response_builder = handler_input.response_builder.speak(
            speech).set_should_end_session(False)
        attr = handler_input.attributes_manager.session_attributes
        attr["state"] = "started_exercises"
        attr["counter"] = 0

        return response_builder.response
