from ask_sdk_runtime.dispatch_components import AbstractRequestHandler, \
    AbstractExceptionHandler


class AllExceptionHandler(AbstractExceptionHandler):
    def can_handle(self, handler_input, exception):
        # type: (HandlerInput, Exception) -> bool
        return True

    def handle(self, handler_input, exception):
        # type: (HandlerInput, Exception) -> Response
        speech = '<voice name="Matthew">Morning workout application is not ready ' \
                 'to handle this for now</voice>' + str(exception)
        handler_input.response_builder.speak(speech).ask(speech)

        return handler_input.response_builder.response
