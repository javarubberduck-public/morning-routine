from ask_sdk_core.utils import is_request_type
from ask_sdk_runtime.dispatch_components import AbstractRequestHandler


class LaunchRequestHandler(AbstractRequestHandler):
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return is_request_type("LaunchRequest")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speech = '<voice name="Matthew">Good sunny, energetic morning family. Welcome to morning ' \
                 'workout application. What do you want to start your morning ' \
                 'workout with?</voice>'
        return handler_input.response_builder.speak(speech).ask(speech) \
            .set_should_end_session(False).response
