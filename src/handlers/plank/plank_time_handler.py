from ask_sdk_core.utils import is_intent_name
from ask_sdk_runtime.dispatch_components import AbstractRequestHandler


class PlankTimeRequestHandler(AbstractRequestHandler):
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return is_intent_name("plankTime")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        if (handler_input.attributes_manager.session_attributes["state"] == "started_plank"):
            minutes = handler_input.request_envelope.request.intent.slots['minutesValue'].value
            seconds = handler_input.request_envelope.request.intent.slots['secondsValue'].value
            speech = f'<voice name="Matthew">Ok, plank exercise will last for {minutes} minutes and {seconds} seconds, starting now</voice>'
            response_builder = handler_input.response_builder.speak(speech) \
                .set_should_end_session(False)
            return response_builder.response
        else:
            return handler_input.response_builder.speak('<voice name="Matthew">Does not work for now</voice>').response
