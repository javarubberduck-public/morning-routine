from ask_sdk_core.utils import is_intent_name
from ask_sdk_runtime.dispatch_components import AbstractRequestHandler


class StartPlankRequestHandler(AbstractRequestHandler):
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return is_intent_name("startPlank")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speech = '<voice name="Matthew">How long do you want to stay in plank?</voice>'
        response_builder = handler_input.response_builder.speak(
            speech).set_should_end_session(False)
        attr = handler_input.attributes_manager.session_attributes
        attr["state"] = "started_plank"
        attr["counter"] = 0

        return response_builder.response
