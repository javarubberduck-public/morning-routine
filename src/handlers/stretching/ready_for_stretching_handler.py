from ask_sdk_core.utils import is_intent_name
from ask_sdk_model.interfaces.audioplayer import PlayDirective, PlayBehavior, \
    AudioItem, Stream, AudioItemMetadata
from ask_sdk_runtime.dispatch_components import AbstractRequestHandler


class ReadyForStretchingHandler(AbstractRequestHandler):
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return is_intent_name("ready")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        if (handler_input.attributes_manager.session_attributes[
            "state"] == "started_stretching"):
            speech = '<voice name="Matthew">Starting now.' \
                     '<break time="10s"/><break time="10s"/><break time="10s"/>' \
                     'Time is up! Say ready for the next exercise.</voice>'
            response_builder = handler_input.response_builder
            response_builder.speak(speech).set_should_end_session(False)
            response_builder.add_directive(PlayDirective(
                PlayBehavior.REPLACE_ALL,
                AudioItem(
                    Stream(
                        token='1',
                        url='https://s3-eu-west-1.amazonaws.com/moring-workout-songs/forest_silence.mp3',
                        offset_in_milliseconds=0
                    ),
                    AudioItemMetadata(
                        title='Waiting for the response',
                        subtitle='forest silence'
                    )
                )
            ))
            return response_builder.response
        else:
            speech = '<voice name="Matthew"> This type of ready is not ' \
                     'supported yet</voice>'
            return handler_input.response_builder.speak(
                speech).set_should_end_session(True).response
