from ask_sdk_core.utils import is_intent_name
from ask_sdk_runtime.dispatch_components import AbstractRequestHandler


class StartStretchingHandler(AbstractRequestHandler):
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return is_intent_name("startStretching")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speech = '<voice name="Matthew"> Ok, we will start stretching whenever you will say ' \
                 '<emphasis level="moderate">Ready</emphasis></voice>'
        handler_input.response_builder.speak(speech).set_should_end_session(False)
        attr = handler_input.attributes_manager.session_attributes
        attr["state"] = "started_stretching"
        attr["counter"] = 0

        return handler_input.response_builder.response
