from ask_sdk_core.skill_builder import SkillBuilder

from handlers.audio.pause_audio_handler import PauseAudioRequestHandler
from handlers.exercises.exercises_version_handler import \
    ExercisesVersionRequestHandler
from handlers.exercises.start_exercises_handler import \
    StartExercisesRequestHandler
from handlers.lifecycle.all_exception_handler import AllExceptionHandler
from handlers.lifecycle.cancel_and_stop_intent_handler import \
    CancelAndStopIntentRequestHandler
from handlers.lifecycle.launch_request_handler import LaunchRequestHandler
from handlers.lifecycle.session_ended_request_handler import \
    SessionEndedRequestHandler
from handlers.plank.plank_time_handler import PlankTimeRequestHandler
from handlers.plank.start_plank_handler import StartPlankRequestHandler
from handlers.stretching.ready_for_stretching_handler import \
    ReadyForStretchingHandler
from handlers.stretching.start_stretching_handler import StartStretchingHandler

sb = SkillBuilder()

# Lifecycle handlers
sb.add_request_handler(LaunchRequestHandler())
sb.add_request_handler(CancelAndStopIntentRequestHandler())
sb.add_request_handler(SessionEndedRequestHandler())
sb.add_exception_handler(AllExceptionHandler())

# Skill handlers
sb.add_request_handler(StartPlankRequestHandler())
sb.add_request_handler(PlankTimeRequestHandler())
sb.add_request_handler(StartExercisesRequestHandler())
sb.add_request_handler(ExercisesVersionRequestHandler())
sb.add_request_handler(StartStretchingHandler())
sb.add_request_handler(ReadyForStretchingHandler())

# Audio handlers
sb.add_request_handler(PauseAudioRequestHandler())


handler = sb.lambda_handler()
